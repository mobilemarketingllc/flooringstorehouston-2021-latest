		<?php do_action('fl_content_close'); ?>
	
	</div><!-- .fl-page-content -->
	<?php 
		
	do_action('fl_after_content'); 
	
	if ( FLTheme::has_footer() ) :
	
	?>
	<footer class="fl-page-footer-wrap" itemscope="itemscope" itemtype="http://schema.org/WPFooter">
		<?php 
			
		do_action('fl_footer_wrap_open');
		do_action('fl_before_footer_widgets');

		
		FLTheme::footer_widgets();
		
		do_action('fl_after_footer_widgets');
		do_action('fl_before_footer');
		
		FLTheme::footer();
		
		do_action('fl_after_footer');
		do_action('fl_footer_wrap_close');
		
		?>
	</footer>
	<?php endif; ?>
	<?php do_action('fl_page_close'); ?>
</div><!-- .fl-page -->
<?php 
	
wp_footer(); 

do_action('fl_body_close');

FLTheme::footer_code();

?>
<!-- <script defer src="https://connect.podium.com/widget.js#API_TOKEN=4a0a9d69-7afd-4cd0-bcc0-2e31150e0c35" id="podium-widget" data-api-token="4a0a9d69-7afd-4cd0-bcc0-2e31150e0c35"></script> -->
</body>
</html>